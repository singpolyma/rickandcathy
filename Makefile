.PHONY: all install clean setup

all: setup
	lazygal --quiet -o dist -s medium=1024x760 -O -t "$$(pwd)/theme/" photos/

install:
	$(RM) -r "$(PREFIX)"/*
	cp -r dist/* "$(PREFIX)"

setup:
	sudo apt-get install -y lazygal

clean:
	$(RM) -r dist
